# dmenu
speedie's dmenu configuration

## Notice

NOTE: This build has [moved](https://codeberg.org/speedie/spmenu) or evolved into
a separate project now called spmenu. spmenu unlike this build is a separate executable
but is otherwise mostly the same as this.

I will no longer update this project, except small bug fixes occasionally. All new features
and scripts will be using spmenu for a few reasons you can see there.

Sorry about that. If you want you can move the `spmenu` binary to `dmenu` to maintain compatibility.

-------------------------------------------------------------------------------------------------------

This is my personal always-changing build of [suckless.org](https://suckless.org)'s [dmenu](https://tools.suckless.org/dmenu).
It is designed to integrate well with [speedwm](https://codeberg.org/speedie/speedwm).

### Special features
This build of dmenu has some features written for this build. Of course if you want, this is free software so you can use it in your own build.

- 256 color support through SGR codes.
- Option to block typing.
- Rewritten arguments, old arguments still work though.
- Border only when centered option
- Hiding each part of the menu

### Other features
Note: This is an incomplete list, it's just here to give you an idea of what this build has to offer.
- Pango markup support.
- Alpha transparency
- Pywal/.Xresources support
- Grid
- Colored Emoji/Font support
- Highlighting
- Case-insensitive by default
- Padding; useful with patched dwm with barpadding or speedwm.
- Fuzzy-finding
- Preselect support
- Line-height
- History support

### Dependencies
- libX11
- libXrender
- freetype
- libXinerama
  - Can be disabled if you don't want/need multi-monitor support.
- tcc compiler (you can swap it out for GCC by passing CC="gcc" to the `make` command if you want)
- Pango (for drawing fonts)
  - If you do not want to use pango, consider my [older dmenu build](https://github.com/speedie-de/dmenu)

### Installation
`# Install dev-vcs/git using your favorite package manager`

`git clone https://codeberg.org/speedie/dmenu`

`cd dmenu/`

`make clean install # Run as root.`

### .Xresources values
This build allows you to define .Xresources values to load on startup. See docs/example.Xresources for a list of default values.

### Scripts
This build of dmenu should work with all dmenu scripts. [Here](https://codeberg.org/speedie/speedwm-extras) are a few I've written/use:

## Notes for users of Arch
This fork of dmenu is compiled using tcc for speed however tcc from the Arch repositories seems to be broken. I'm sure there's a better way to fix this but I just fix it by installing [this package](https://aur.archlinux.org/packages/tcc-ziyao) from the AUR.
