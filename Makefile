# dmenu

# See LICENSE file for copyright and license details.

include options.mk

SRC = draw.c dmenu.c main.c
OBJ = $(SRC:.c=.o)

all: options dmenu

options:
	@echo dmenu build options:
	@echo "CFLAGS   = $(CFLAGS)"
	@echo "LDFLAGS  = $(LDFLAGS)"
	@echo "CC       = $(CC)"

.c.o:
	$(CC) -c $(CFLAGS) -g $<


$(OBJ): arg.h options.h options.mk draw.h

dmenu: dmenu.o draw.o main.o
	$(CC) -o $@ dmenu.o draw.o main.o $(LDFLAGS)

clean:
	rm -f dmenu $(OBJ) dmenu-spde-$(VERSION).tar.gz

dist: clean
	mkdir -p dmenu-spde-$(VERSION)
	cp -r LICENSE Makefile *.h options.mk *.c docs/ scripts/ dmenu-spde-$(VERSION)
	tar -cf dmenu-spde-$(VERSION).tar dmenu-spde-$(VERSION)
	gzip dmenu-spde-$(VERSION).tar
	rm -rf dmenu-spde-$(VERSION)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f dmenu scripts/* $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/dmenu*
	rm -f *.o
	rm -f dmenu

uninstall:
		  $(DESTDIR)$(PREFIX)/bin/dmenu*\

help:
	@echo install: Installs dmenu. You may need to run this as root.
	@echo uninstall: Uninstalls dmenu. You may need to run this as root.
	@echo help: Displays this help sheet.

.PHONY: all options clean dist install uninstall help
